# CS 61A: Structure and Interpretation of Computer Programs

- [CS 61A](https://cs61a.org/)
- [CS 61A - Spring 2021](https://inst.eecs.berkeley.edu/~cs61a/sp21/)
- [CS 61A - Spring 2021 - Video](https://www.bilibili.com/video/BV1v64y1Q78o)
- [Composing Programs - CS 61A Textbook](https://composingprograms.com/)
- [Guide to CS 61A](https://cs61a.vanshaj.dev/)

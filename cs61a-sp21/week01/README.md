# week 01

- [Date 1/20](#date-120)
- [Date 1/22](#date-122)
- [Lab 0: Getting Started](#lab-0-getting-started)

## Date 1/20

- Textbook - ch1.1
- [Lab 00: Getting Started](https://inst.eecs.berkeley.edu/~cs61a/sp21/lab/lab00/)

## Date 1/22

- Textbook - ch1.2, ch1.3, ch1.4
- [HW 01: Variables & Functions, Control](https://inst.eecs.berkeley.edu/~cs61a/sp21/hw/hw01/)

## Lab 0: Getting Started

1. 需要下载 `lab00.zip` 文件
2. 最好在 Linux 平台下操作
3. 安装终端模拟器、Python、文本编辑器
4. 终端基本用法
5. 解压 `lab00.zip` 文件并移动到 `cs61a/lab` 目录内

### 课程目录结构

```bash
cs61a
|-lab
|-projects
```

### 作业和实验

- 解锁特定问题：`python3 ok --local -u -q python-basics` (解锁 `python-basics` 问题)
- 编辑 Python 源代码文件，在函数里面有问题描述，需要将 `______` 改为合适的内容
- 调用 `python3 ok --local` 运行测试来检查答案是否正确

> 注意：使用 `--local` 选项可以不提交答案给网站，而是在本地直接检查

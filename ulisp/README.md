# ulisp

原作是用 JavaScript 实现的将类 lisp 语言编译成 LLVM 和 x86 的汇编语言；现在使用 Python 重新实现一遍，了解相关的编译知识：

- [译文 1](https://gitlab.com/xxwowxx/misc/-/blob/main/ulisp/translation/compiler-basics-lisp-to-assembly.md)

## [原文链接 (ulisp)](https://github.com/eatonphil/ulisp)

1. [Lisp to assembly](https://notes.eatonphil.com/compiler-basics-lisp-to-assembly.html)
2. [User-defined functions and variables](https://notes.eatonphil.com/compiler-basics-functions.html)
3. [LLVM](https://notes.eatonphil.com/compiler-basics-llvm.html)
4. [LLVM conditionals and compiling fibonacci](https://notes.eatonphil.com/compiler-basics-llvm-conditionals.html)
5. [LLVM system calls](https://notes.eatonphil.com/compiler-basics-llvm-system-calls.html)
6. [An x86 upgrade](https://notes.eatonphil.com/compiler-basics-an-x86-upgrade.html)

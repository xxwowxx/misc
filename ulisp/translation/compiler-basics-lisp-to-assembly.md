# Writing a lisp compiler from scratch in JavaScript: 1. lisp to assembly

本文介绍如何使用 JavaScript 实现一个简单的编译器，而且不使用第三方库。我们的目标是：获取类似 `(+ 1 (+ 2 3))` 形式的输入，然后生成对应的汇编程序 (该程序会执行一些操作，再将 `6` 作为退出码返回)。最终的编译器代码可在[这里](https://github.com/eatonphil/ulisp)找到。

本文涵盖的内容：

- 解析 (Parsing)
- 代码生成 (Code generation)
- 汇编基础 (Assembly basics)
- 系统调用 (Syscalls)

本文所忽略的内容：

- 可编程的函数定义 (Programmable function definitions)
- 没有符号和数字这两种数据类型 (Non-symbol/-numeric data types)
- 超过 3 个参数的函数 (More than 3 function arguments)
- 安全性 (Lots of safety)
- 错误消息 (Lots of error messages)

## 解析 (Parsing)

我们会采用先前提到的 S 表达式语法，因为解析它的方法非常容易。而且，我们的输入语言的复杂度十分有限，所以也不会将我们的解析器拆分成独立的词法阶段和语法阶段。

> 如果你需要支持字符串字面量、注释、十进制数字面量，以及其他更复杂的字面量；分离出独立的词法阶段和语法阶段，会更容易实现解析器。
>
> 如果你对解析的各个独立阶段都感兴趣，可以阅读我写的 [writing a JSON parser](https://notes.eatonphil.com/writing-a-simple-json-parser.html) 文章。
>
> 或者，你可以前往我的 [BSDScheme](https://github.com/eatonphil/bsdscheme) 项目查看为 Scheme 编写且功能完整的[词法分析器](https://github.com/eatonphil/bsdscheme/blob/master/src/lex.d)和[语法分析器](https://github.com/eatonphil/bsdscheme/blob/master/src/parse.d)。

解析器 (Parser) 应该生成一棵抽象语法树 (Abstract Syntax Tree, AST)，这是代表输入程序的一种数据结构。明确地说，我们希望将输入的 `(+ 1 (+ 2 3))` 转换为 JavaScript 的 `['+', 1, ['+', 2, 3]]` 形式。

有不同的方法可以实现解析，但是对我来说最直接的方法还是：实现一个函数，接收一个程序 (字符串)，返回一个 tuple (目前已解析的程序) + 尚未解析的程序 (字符串)。

大体的函数结构如下：

```javascript
module.exports.parse = function parse(program) {
  const tokens = [];

  ... logic to be added ...

  return [tokens, ''];
};
```

由上述函数结构可知，第一次调用 `parse` 函数的代码，必须解包最外层的 tuple，才能得到 AST。而且，通过检查返回值的第二个元素是不是空字符串，可以检查整个程序是否被完全解析。

在函数内部，我们会迭代每个字符，然后不断累积拼接，除非遇到了空格、左圆括号、右圆括号：

```javascript
module.exports.parse = function parse(program) {
  const tokens = [];
  let currentToken = '';

  for (let i = 0; i < program.length; i++) {
    const char = program.charAt(i);

    switch (char) {
      case '(': // TODO
        break;
      case ')': // TODO
        break;
      case ' ':
        tokens.push(+currentToken || currentToken);
        currentToken = '';
        break;
      default:
        currentToken += char;
        break;
    }
  }

  return [tokens, ''];
};
```

递归的部分总是最有挑战的。右圆括号的处理是最简单的。我们必须将当前 token 压入到 tuple 内，然后返回剩余程序的所有 token：

```javascript
module.exports.parse = function parse(program) {
  const tokens = [];
  let currentToken = '';

  for (let i = 0; i < program.length; i++) {
    const char = program.charAt(i);

    switch (char) {
      case '(': // TODO
        break;
      case ')':
        tokens.push(+currentToken || currentToken);
        return [tokens, program.substring(i + 1)];
      case ' ':
        tokens.push(+currentToken || currentToken);
        currentToken = '';
        break;
      default:
        currentToken += char;
        break;
    }
  }

  return [tokens, ''];
};
```

最终左圆括号部分应该递归，将解析出来的 token 添加到兄弟 token list 里面，然后强制循环在新的未解析点处开始：

```javascript
module.exports.parse = function parse(program) {
  const tokens = [];
  let currentToken = '';

  for (let i = 0; i < program.length; i++) {
    const char = program.charAt(i);

    switch (char) {
      case '(': {
        const [parsed, rest] = parse(program.substring(i + 1));
        tokens.push(parsed);
        program = rest;
        i = 0;
        break;
      }
      case ')':
        tokens.push(+currentToken || currentToken);
        return [tokens, program.substring(i + 1)];
      case ' ':
        tokens.push(+currentToken || currentToken);
        currentToken = '';
        break;
      default:
        currentToken += char;
        break;
    }
  }

  return [tokens, ''];
};
```

假设上述代码都在 `parser.js` 文件内，让我们在 REPL 里面测试一下：

```bash
$ node
> const { parse } = require('./parser');
undefined
> console.log(JSON.stringify(parse('(+ 3 (+ 1 2)')));
[[["+",3,["+",1,2]]],""]
```

## 汇编 101

汇编语言实际上是我们能够使用的最底层编程语言。汇编语言人类可读的编程语言，它与 CPU 能够解释的机器语言是 1:1 对应的。将汇编代码转换为机器代码的过程由汇编器进行；反向过程则由反汇编器所实现。我们使用 `gcc` 执行汇编过程。

在汇编语言里面，主要的数据结构由寄存器和程序栈所组成。程序内的每个函数都能访问到相同的寄存器，但是根据函数约定可以划分出独立的栈空间，所以栈保存的内容比寄存器的内容往往更久。`RAX`、`RDI`、`RDX`、`RSI` 是我们能够使用的一小部分寄存器。

现在，我们只需要一小部分指令来编译我们的程序：

- `MOV`：将一个寄存器的内容存储到另一个寄存器里面，或者将数字字面量存储到寄存器中
- `ADD`：两个寄存器求和，再将结果存入到第一个寄存器中
- `PUSH`：将寄存器的内容存到栈空间内
- `POP`：从栈顶移除值，并且存到一个寄存器内
- `CALL`：进入一个新的栈空间，然后开始执行函数
- `RET`：返回到调用函数栈，然后执行函数调用的下一条指令
- `SYSCALL`：与 `CALL` 类似，只是该函数由内核处理

## 函数调用约定

汇编语言如此灵活，以至于无法在语言层面上构造函数调用。因此，至少需要回答以下问题：

- 调用者将参数存储在哪里？而被调用者又该如何访问这些参数？
- 被调用者将返回值存储在哪里？而调用者又该如何访问返回值？
- 寄存器的内容由谁 (调用者/被调用者) 负责存储？

为了避免陷入到细节中，我们假设目标平台是 x86_64 的 macOS/Linux 操作系统：

- 参数按顺序存储在 `RDI`、`RSI`、`RDX` 寄存器 (因此不支持数量超过 3 个的参数传递)
- 返回值存储在 `RAX` 寄存器
- `RDI`、`RSI`、`RDX` 寄存器由调用者负责存储

## 代码生成

根据汇编基础和函数调用约定，我们已经可以根据 AST 生成代码了。

实现编译功能的代码结构如下：

```javascript
function emit(depth, code) {
  const indent = new Array(depth + 1).map(() => '').join('  ');
  console.log(indent + code);
}

function compile_argument(arg, destination) {
  // If arg AST is a list, call compile_call on it

  // Else must be a literal number, store in destination register
}

function compile_call(fun, args, destination) {
  // Save param registers to the stack

  // Compile arguments and store in param registers

  // Call function

  // Restore param registers from the stack

  // Move result into destination if provided
}

function emit_prefix() {
  // Assembly prefix
}

function emit_postfix() {
  // Assembly postfix
}

module.exports.compile = function parse(ast) {
  emit_prefix();
  compile_call(ast[0], ast.slice(1));
  emit_postfix();
};
```

根据注释的伪代码，我们可以很容易地填入所有代码，除了 prefix 和 postfix 这两部分的代码：

```javascript
function compile_argument(arg, destination) {
  // If arg AST is a list, call compile_call on it
  if (Array.isArray(arg)) {
    compile_call(arg[0], arg.slice(1), destination);
    return;
  }

  // Else must be a literal number, store in destination register
  emit(1, `MOV ${destination}, ${arg}`);
}

const BUILTIN_FUNCTIONS = { '+': 'plus' };
const PARAM_REGISTERS = ['RDI', 'RSI', 'RDX'];

function compile_call(fun, args, destination) {
  // Save param registers to the stack
  args.forEach((_, i) => emit(1, `PUSH ${PARAM_REGISTERS[i]}`));

  // Compile arguments and store in param registers
  args.forEach((arg, i) => compile_argument(arg, PARAM_REGISTERS[i]));

  // Call function
  emit(1, `CALL ${BUILTIN_FUNCTIONS[fun] || fun}`);

  // Restore param registers from the stack
  args.forEach((_, i) => emit(1, `POP ${PARAM_REGISTERS[args.length - i - 1]}`));

  // Move result into destination if provided
  if (destination) {
    emit(1, `MOV ${destination}, RAX`);
  }

  emit(0, ''); // For nice formatting
}
```

在更好的编译器中，我们不会将 `plus` 作为内建函数。我们会使用 `ADD` 汇编指令生成汇编代码。然而，创建一个 `plus` 函数不仅使得代码生成更加简单，还可以看一下函数调用的代码结构是怎样的。

我们会在 prefix 部分定义 `plus` 内建函数。

## prefix 部分

汇编程序的某些 section 会加载到内存中。其中最重要的就是 `text` 和 `data` 这两个 section。`text` 是只读的，用于存储程序指令。CPU 会在 `text` 的某处位置开始执行指令，然后不断获取下一条指令并执行，除非跳转到其他位置并执行该处的指令 (比如：`CALL`、`RET`、`JMP`)。

为了指示出 text section 范围，会在 prefix 部分使用 `.text` 伪指令，而且该部分在代码生成部分之前。

> 在 data section 部分，存储的是静态初始化值 (比如：全局变量)。我们现在不需要该部分，因此会忽略它。
>
> [这里](https://www.cs.bgu.ac.il/~caspl122/wiki.files/lab2/ch07lev1sec6/ch07lev1sec6.html)介绍了这些 section 的相关内容。

另外，我们还需要设置程序入口点 (这里使用 `_main` 作为入口点)，还要添加 (`.global _main`) 伪指令；这样才能让入口点变成外部可见的。这一步很重要，因为要使用 `gcc` 生成可执行文件，而 `gcc` 要求能够访问到入口点。

至此，prefix 部分的代码如下：

```javascript
function emit_prefix() {
  emit(1, '.global _main\n');

  emit(1, '.text\n');

  // TODO: add built-in functions

  emit(0, '_main:');
}
```

我们的 prefix 现在还剩下最后的一部分，就是添加 plus 内建函数的实现。为此，我们使用前两个参数寄存器 (`RDI`、`RSI`)和返回值寄存器 (`RAX`)：

```javascript
function emit_prefix() {
  emit(1, '.global _main\n');

  emit(1, '.text\n');

  emit(0, 'plus:');
  emit(1, 'ADD RDI, RSI');
  emit(1, 'MOV RAX, RDI');
  emit(1, 'RET\n');

  emit(0, '_main:');
}
```

至此，我们完成了该部分的功能。

## postfix 部分

postfix 部分负责的工作很简单，设置 `RAX` 寄存器的值，然后调用 `exit` 函数，因为这是程序最后调用的函数结果。

`exit` 是系统调用，所以我们使用 `SYSCALL` 调用它。在 x86_64 的 macOS/Linux 平台上，调用 `SYSCALL` 的参数约定与 `CALL` 的一样。但是我们还需要告诉 `SYSCALL` 要调用的是哪个系统调用。这个约定就是将 `RAX` 寄存器设置为整数，而且该整数代表当前系统的系统调用号。在 Linux 中是 `60`，在 macOS 中则是 `0x2000001`。

> 当我在说“约定”时，并不是说你真的有可选余地。操作系统和标准库所选的约定是很随意的。但是如果你想要编写一个可工作的程序，而且需要使用系统调用或调用 glibc 时，你必须遵循这些约定。

postfix 部分如下：

```javascript
const os = require('os');

const SYSCALL_MAP = os.platform() === 'darwin' ? {
    'exit': '0x2000001',
} : {
    'exit': 60,
};

function emit_postfix() {
  emit(1, 'MOV RDI, RAX'); // Set exit arg
  emit(1, `MOV RAX, ${SYSCALL_MAP['exit']}`); // Set syscall number
  emit(1, 'SYSCALL');
}
```

## 将各个部分组合在一起

最后，我们编写 JavaScript 的入口点，接着配合样例程序运行编译器。

入口点最终看起来像这个：

```javascript
const { parse } = require('./parser');
const { compile } = require('./compiler');

function main(args) {
  const script = args[2];
  const [ast] = parse(script);
  compile(ast[0]);
}

main(process.argv);
```

运行结果如下：

```bash
$ node ulisp.js '(+ 3 (+ 2 1))'
  .global _main

  .text

plus:
  ADD RDI, RSI
  MOV RAX, RDI
  RET

_main:
  PUSH RDI
  PUSH RSI
  MOV RDI, 3
  PUSH RDI
  PUSH RSI
  MOV RDI, 2
  MOV RSI, 1
  CALL plus
  POP RSI
  POP RDI
  MOV RSI, RAX

  CALL plus
  POP RSI
  POP RDI

  MOV RDI, RAX
  MOV RAX, 0x2000001
  SYSCALL
```

## 根据输出的汇编程序生成可执行文件

我们可以将编译器的输出重定向到汇编文件中，然后调用 `gcc` 生成可执行文件。我们可以生成可以运行的程序。调用 `echo $?` 命令可以查看上一个进程的退出码：

```bash
$ node ulisp.js '(+ 3 (+ 2 1))' > program.S
$ gcc -mstackrealign -masm=intel -o program program.s
$ ./program
$ echo $?
6
```

现在，我们得到了一个可以运行的编译器！编译器的完整代码在[这里](https://github.com/eatonphil/ulisp)。

## 延申阅读

- [x86_64 calling convention](https://aaronbloomfield.github.io/pdr/book/x86-64bit-ccc-chapter.pdf)
- macOS assembly programming
  - [Stack alignment on macOS](http://fabiensanglard.net/macosxassembly/index.php)
  - [Syscalls on macOS](https://filippo.io/making-system-calls-from-assembly-in-mac-os-x/)
- Destination-driven code generation
  - [Kent Dybvig's original paper](https://www.cs.indiana.edu/~dyb/pubs/ddcg.pdf)
  - [One-pass code generation in V8](http://cs.au.dk/~mis/dOvs/slides/46b-codegeneration-in-V8.pdf)
